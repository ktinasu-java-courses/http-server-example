package org.example.app.manager;

import org.example.app.domain.Apartment;
import org.example.app.dto.*;
import org.example.framework.auth.AnonymousPrincipal;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

public class AvitoManager {
    private final List<Apartment> items = new ArrayList<>(100);

    public List<AvitoGetAllRS> getAll(final Principal principal) {
        return items.stream()
                .map(o -> new AvitoGetAllRS(
                        o.getId(),
                        o.getOwner(),
                        o.getRoomsNumber(),
                        o.isStudio(),
                        o.isFreeLayout(),
                        o.getPrice(),
                        o.getArea(),
                        o.isHasBalcony(),
                        o.isHasLoggia(),
                        o.getFloorNumber(),
                        o.getFloorNumberInHouse(),
                        o.getAddress(),
                        o.getDescription()
                ))
                .collect(Collectors.toList());
    }

    public AvitoGetByIdRS getById(final AvitoGetByIdRQ requestDTO, final Principal principal) {
        Optional<Apartment> result = items.stream()
                .filter(o -> o.getId().equals(requestDTO.getId()))
                .findAny();
        final Apartment apartment = result.orElse(null);
        if (apartment != null) {
            return new AvitoGetByIdRS(
                    apartment.getId(),
                    apartment.getOwner(),
                    apartment.getRoomsNumber(),
                    apartment.isStudio(),
                    apartment.isFreeLayout(),
                    apartment.getPrice(),
                    apartment.getArea(),
                    apartment.isHasBalcony(),
                    apartment.isHasLoggia(),
                    apartment.getFloorNumber(),
                    apartment.getFloorNumberInHouse(),
                    apartment.getAddress(),
                    apartment.getDescription());
        } else {
            return new AvitoGetByIdRS();
        }
    }

    public AvitoCreateRS create(final AvitoCreateRQ requestDTO, final Principal principal) {
        if (principal.getName().equals(AnonymousPrincipal.ANONYMOUS)) {
            return null;
        }
        final Apartment item = new Apartment(
                UUID.randomUUID().toString(),
                principal.getName(),
                requestDTO.getRoomsNumber(),
                requestDTO.isStudio(),
                requestDTO.isFreeLayout(),
                requestDTO.getPrice(),
                requestDTO.getArea(),
                requestDTO.isHasBalcony(),
                requestDTO.isHasLoggia(),
                requestDTO.getFloorNumber(),
                requestDTO.getFloorNumberInHouse(),
                requestDTO.getAddress(),
                requestDTO.getDescription()
        );

        items.add(item);

        return new AvitoCreateRS(
                item.getId(),
                item.getRoomsNumber(),
                item.isStudio(),
                item.isFreeLayout(),
                item.getPrice(),
                item.getArea(),
                item.isHasBalcony(),
                item.isHasLoggia(),
                item.getFloorNumber(),
                item.getFloorNumberInHouse(),
                item.getAddress(),
                item.getDescription()
        );
    }

    public AvitoUpdateRS update(final AvitoUpdateRQ requestDTO, final Principal principal) {
        Apartment item = validityCheck(principal, requestDTO.getId());
        if (item == null) {
            return null;
        }
        final int idx = getIdxById(requestDTO.getId());

        items.set(idx, new Apartment(requestDTO.getId(),
                principal.getName(),
                requestDTO.getRoomsNumber(),
                requestDTO.isStudio(),
                requestDTO.isFreeLayout(),
                requestDTO.getPrice(),
                requestDTO.getArea(),
                requestDTO.isHasBalcony(),
                requestDTO.isHasLoggia(),
                requestDTO.getFloorNumber(),
                requestDTO.getFloorNumberInHouse(),
                requestDTO.getAddress(),
                requestDTO.getDescription()));

        return new AvitoUpdateRS(
                requestDTO.getId(),
                requestDTO.getRoomsNumber(),
                requestDTO.isStudio(),
                requestDTO.isFreeLayout(),
                requestDTO.getPrice(),
                requestDTO.getArea(),
                requestDTO.isHasBalcony(),
                requestDTO.isHasLoggia(),
                requestDTO.getFloorNumber(),
                requestDTO.getFloorNumberInHouse(),
                requestDTO.getAddress(),
                requestDTO.getDescription()
        );
    }

    public AvitoDeleteByIdRS deleteById(final AvitoDeleteByIdRQ requestDTO, final Principal principal) {
        Apartment item = validityCheck(principal, requestDTO.getId());
        if (item == null) {
            return null;
        }
        int idx = getIdxById(requestDTO.getId());
        if (idx == -1) {
            return null;
        }
        items.remove(idx);
        return new AvitoDeleteByIdRS(
                item.getId(),
                item.getRoomsNumber(),
                item.isStudio(),
                item.isFreeLayout(),
                item.getPrice(),
                item.getArea(),
                item.isHasBalcony(),
                item.isHasLoggia(),
                item.getFloorNumber(),
                item.getFloorNumberInHouse(),
                item.getAddress(),
                item.getDescription()
        );
    }

    private Apartment validityCheck(Principal principal, String requestDTOId) {
        if (principal.getName().equals(AnonymousPrincipal.ANONYMOUS)) {
            return null;
        }
        Apartment item = null;
        for (Apartment currentItem : items) {
            if (currentItem.getId().equals(requestDTOId)) {
                item = currentItem;
            }
        }
        if (item == null) {
            return null;
        }
        if (!principal.getName().equals(item.getOwner())) {
            return null;
        }
        return item;
    }

    private int getIdxById(String id) {
        int idx = -1;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(id)) {
                idx = i;
            }
        }
        return idx;
    }
}
