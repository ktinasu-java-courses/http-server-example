package org.example.app;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.app.handler.AvitoHandler;
import org.example.app.manager.AvitoManager;
import org.example.framework.http.HttpMethods;
import org.example.framework.http.Server;
import org.example.framework.middleware.AnonAuthMiddleware;
import org.example.framework.middleware.X509AuthNMiddleware;
import org.example.framework.util.Maps;

import java.io.IOException;
import java.util.regex.Pattern;

@Slf4j
public class Main {
    public static void main(String[] args) {
        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "passphrase");
        System.setProperty("javax.net.ssl.trustStore", "web-certs/truststore.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "passphrase");

        final AvitoManager manager = new AvitoManager();

        final Gson gson = new Gson();
        final AvitoHandler avitoHandler = new AvitoHandler(gson, manager);

        final String regex = "(?<apartmentId>[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})$";
        final Server server = Server.builder()
                .middleware(new X509AuthNMiddleware())
                .middleware(new AnonAuthMiddleware())
                .routes(
                        Maps.of(
                                Pattern.compile("^/avito$"), Maps.of(
                                        HttpMethods.GET, avitoHandler::getAll,
                                        HttpMethods.POST, avitoHandler::create
                                ),
                                Pattern.compile("^/avito/" + regex), Maps.of(
                                        HttpMethods.GET, avitoHandler::getById,
                                        HttpMethods.DELETE, avitoHandler::deleteById,
                                        HttpMethods.PUT, avitoHandler::update
                                )
                        )
                )
                .build();

        final int port = 8443;
        try {
            server.serveHTTPS(port);
        } catch (IOException e) {
            log.error("can't serve", e);
        }
    }
}

